import React, { useEffect, useState } from 'react'
import Layout from '../components/layout'
import {Route} from "react-router-dom";
import ItemCreate from '../components/layout/jogo/create';
import Itemlist from '../components/layout/jogo/list';
import jwt from 'jsonwebtoken'
import { getToken } from '../config/auth';

const User = (props) => {
    const [useInfo, setUseInfo] = useState({})    
    useEffect(() => {
        (async() => {
            const {admin} = await jwt.decode(getToken())
            setUseInfo(admin)
        })()
        return () => { }        
    },[])

    return(
        <Layout info={useInfo}>          
            <Route exact path={props.match.path} component={Itemlist}/>
            <Route exact path={props.match.path + "create"} component={ItemCreate}/>
            <Route exact path={props.match.path + "edit/:jogoId"} component={ItemCreate}/>          
        </Layout>
    )
}

export default User