import React from 'react';
import Layout from '../components/layout';
import LoginComponent from '../components/layout/login/login'


const Login = () => {
  return (
    <Layout>
      <LoginComponent />
    </Layout>
  )
}

export default Login;