import React, { useEffect, useState } from 'react'
import Layout from '../components/layout'
import {Route} from "react-router-dom";
import UserCreate from '../components/layout/user/create';
import Userlist from '../components/layout/user/list';
import jwt from 'jsonwebtoken'
import { getToken } from '../config/auth';

const Admin = (props) => {
    const [useInfo, setUseInfo] = useState({})    
    useEffect(() => {
        (async() => {
            const {admin} = await jwt.decode(getToken())
            setUseInfo(admin)
        })()
        return () => { }        
    },[])

    return(
        <Layout info={useInfo}>          
            <Route exact path={props.match.path + "list"} component={Userlist}/>
            <Route exact path={props.match.path + "create"} component={UserCreate}/>
            <Route exact path={props.match.path + "edit/:userId"} component={UserCreate}/>          
        </Layout>
    )
}

export default Admin