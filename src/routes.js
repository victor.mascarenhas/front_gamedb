import React from 'react'
import {isAuthenticated} from './config/auth'
import ViewAdmin from './views/admin'
import ViewUser from './views/user'
import ViewLogin from './views/login'
import ErrorHandler from './views/errors/error'
import ViewCreate from './views/create'
import {
  Switch,
  Route,
  Redirect,
  Router
} from "react-router-dom";
import history from './config/history'


const CustomRoute = ({ ...rest }) => {
  if(!isAuthenticated()) {
  return <Redirect to='/login' />
  }
  return <Route {...rest} />
}

const Routers = () => (
  <Router history={history}>
    <Switch>
        <Route exact path="/login" component={ViewLogin}/>
        <Route exact path="/user/create" component={ViewCreate}/>
        <Route path="/erro/:erro" component={ErrorHandler}/>
        <CustomRoute path="/admin/" component={ViewAdmin}/>
        <CustomRoute path="/" component={ViewUser}/>
        
    </Switch>
  </Router>

)
  export default Routers