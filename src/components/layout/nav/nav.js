import React, {useEffect, useState} from 'react'
import { useHistory} from 'react-router-dom'
import jwt from 'jsonwebtoken'
import {getToken, isAuthenticated} from '../../../config/auth'
import './nav.css'

const Nav = (props) => {    
    const [userIsAdmin, setUserIsAdmin] = useState({})        

    const history = useHistory()
    const changePage =() => history.push(props.to) 

    const changelist = () => {    
    if(props.lista === "Usuários"){
        history.push('/admin/list')
    }else{history.push('/') }} 

                              
    useEffect(() => {
        (async () => {
            if (isAuthenticated()){
            const {admin} = await jwt.decode(getToken())    
            setUserIsAdmin(admin.is_admin)}
            else{setUserIsAdmin(false)}
        })()
        return () => {}
    }, [])

    return(
    <nav>
    <div className="title"> {props.active} </div>
    <div className="action"> 
        <button onClick={changePage}> {props.name} </button>
       {userIsAdmin ? <button onClick={changelist}> {props.lista} </button> : "" }       
    </div>
    </nav>
)}

export default Nav