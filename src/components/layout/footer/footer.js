import React from 'react'
import imgLogo from '../../../assets/img/game.png';
import './footer.css';

const Footer = () => (
    <footer>
    <div className="projectName">
        GameDB - Cadastro de jogos
    </div>
    <div className="copyRight">
        <img src={imgLogo} alt="logo" />
    </div>
    </footer>
) 

export default Footer