import React, { useState, useEffect } from 'react';
import './style.css'

const Alert = (props) => {
    
const [isShow, setShow] = useState(props.show || false)

const closeAlert = () => {
    setShow(false)
}

useEffect(() => {
    setShow(props.show)
    return () => setShow(false)
},[props.show]
)

const createAlert = (type, icon, message, msgRedirect = false) => (
    isShow ? 
    (
                <div className={`alert alert-${type}`}>
                    <span>
                        <i className={`fa fa-${icon}`}></i> {message}
                        <br />
                        {msgRedirect ? (<small> Você será redirecionado para a lista </small>) : ""}
                    </span>
                    <div style={{ cursor: 'pointer' }} onClick={closeAlert}><i className="fa fa-close" /></div>
                </div>            
    )
    : ""    
)

    const checkAlert =() => {
        switch (props.type) {
            case 'success':
                return createAlert('success', 'check', props.message, true);
            case 'error':
                return createAlert('error', 'warning', props.message, false);
                        
            default:
                return createAlert('info', 'question-circle', 'Algo inesperado aconteceu, tente novamente.', false);
                
        }
    } 


    return (
    <React.Fragment>
        <div className="boxAlert">
        {checkAlert()}
        </div>
    </React.Fragment>
    )        
    
}

export default Alert