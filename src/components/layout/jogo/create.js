import React, {useState, useEffect} from 'react'
import {createItem, EditItem, ShowItem} from '../../../services/jogos'
import './jogo.css'
import Loading from '../loading/loading'
import Alert from '../alert'
import Nav from '../nav/nav'
import {useHistory, useParams} from 'react-router-dom'

const ItemCreate = (props) => {

const [isSubmit, setIsSubmit] = useState(false)
const [isEdit, setIsEdit] = useState(false)
const [alert, setAlert] = useState({})
const history = useHistory()
const { jogoId } = useParams()
const [form, setForm] = useState({})
const methodForm = isEdit ? EditItem : createItem

useEffect(() => {
    
    const getItem = async () => {
        const jogo = await ShowItem(jogoId)
        if (jogo.data.Dtlanc) {
            delete jogo.data.Dtlanc
        }
        setForm(jogo.data)
    }
    if (jogoId) {
        setIsEdit(true)
        getItem()
    }
},[jogoId])

const handleChange = (event) => {         
    setForm({
        ...form,
        [event.target.name]: event.target.value
    })
}

const formIsValid = () => {
    return form.nome && form.Dtlanc && form.mode && form.plat && form.gen && form.dev
}

const submitForm = async (event) => {
    try{
        setIsSubmit(true)
        console.log('Enviando...', form)
        await methodForm(form)
        setAlert({
            type: "success",
            message: "seu formulário foi enviado com sucessso",
            show: true
        })        
        setForm({})
        setIsSubmit(false)
        setTimeout(() =>  history.push('/'), 1500)       
    } catch (e) {
        if(e.response.data.errors){
            var err = e.response.data.errors.map(item => item.msg)
            console.log(err)
            setIsSubmit(false)
            setAlert({
                type: "error",
                message: `${err}`,
                show: true
            })
    }}
}
    
return (
    <section>
        <Nav name="Lista" to="/" active="Incluir/Atualizar" />
        <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || ""} />
            <div className="create_user">
                
                <div className="form_login">
                    <div>
                        <label htmlFor="auth_nome">Nome do jogo:</label>
                        <input disabled={isSubmit} type="text" id="auth_nome" name="nome" onChange={handleChange} value={form.nome || ""} placeholder="Insira o nome do jogo"/>
                    </div>
                    <div>
                        <label htmlFor="auth_Dtlanc">Data de lançamento:</label>
                        <input disabled={isSubmit} type="date" id="auth_Dtlanc" name="Dtlanc" onChange={handleChange} value={form.Dtlanc || ""} placeholder="Insira a data de lançamento"/>
                    </div>
                    <div>
                        <label htmlFor="auth_mode">Modo de jogo:</label>
                        <input disabled={isSubmit} type="text" id="auth_mode" name="mode" onChange={handleChange} value={form.mode || ""} placeholder="Exemplo: Single Player"/>
                    </div>
                    <div>
                        <label htmlFor="auth_plat">Plataforma:</label>
                        <input disabled={isSubmit} type="text" id="auth_plat" name="plat" onChange={handleChange} value={form.plat || ""} placeholder="Exemplo: Playstation 4"/>
                    </div>
                    <div>
                        <label htmlFor="auth_gen">Gênero:</label>
                        <input disabled={isSubmit} type="text" id="auth_gen" name="gen" onChange={handleChange} value={form.gen || ""} placeholder="Exemplo: Survival Horror"/>
                    </div> 
                    <div>
                        <label htmlFor="auth_dev">Desenvolvedor:</label>
                        <input disabled={isSubmit} type="text" id="auth_dev" name="dev" onChange={handleChange} value={form.dev || ""} placeholder="Exemplo: Red Barrels Inc."/>
                    </div>                        
                    <button disabled={!formIsValid()} onClick={submitForm}>{ isEdit ? 'Atualizar' : 'Cadastrar' } </button>
                    <Loading show={isSubmit}/>                               
                </div>
                
            </div>   
            </section>     
)

}

export default ItemCreate