import React, { useEffect, useState } from 'react'
import {listItem} from '../../../services/jogos'
import './jogo.css'
import Confirmation from '../confirmation'
import Loading from '../loading/loading'
import Nav from '../nav/nav'


const Itemlist = (props) => {
    const [jogo, setJogo] = useState([])
    const [confirmation, setConfirmation] = useState({
        isShow: false,
        params: {}        
    })           

    const getList = async () => {
        try{            
            const jogo = await listItem()
            setJogo(jogo.data)            
        }catch (error) {
            console.log('error', error)
        }
        }
   
        const editItem = (jogo) => props.history.push(`/edit/${jogo._id}`)
        
        const deleteItem = (jogo) => setConfirmation({
            isShow: true,
            params: jogo
        })

        /*const deleteItem = async ({nome}) => {                           
            try{
                if(window.confirm(`Você deseja excluir ${nome}?`)){
                await DeleteItem(nome)
                getList()
                }
            }catch(error){
                console.log(error)
            }
}*/

    const isEmpty = jogo.length === 0
    const mountList = () => {
    
    const linhas = jogo.map((it, index) => (
            <tr key={index}>
                <td>{it.nome}</td>
                <td>{it.plat}</td>
                <td>{it.dev}</td>
                <td>{it.mode}</td>
                <td>{it.gen}</td>
                <td>
                    <span onClick={() => editItem(it)}> Editar </span>
                    <span onClick={() => deleteItem(it)}>| Excluir </span>
                </td>
            </tr>
        ))
        return !isEmpty ? (
            <table border="1">
            <thead>
            <tr>
                <th>NOME</th>
                <th>PLATAFORMA</th>
                <th>DESENVOLVEDOR</th>
                <th>MODO DE JOGO</th>
                <th>GÊNERO</th>
                <th>AÇÕES</th>
            </tr>
            </thead>
            <tbody>
                {linhas}
            </tbody>
            </table> 
        ) : ""
    }
    
        useEffect(() => {
            getList()
        }, [])

        useEffect(() => {
            getList()
        }, [confirmation])

    return (
        <React.Fragment>
            <Nav name="Novo" to="/Create" lista="Usuários" active="Jogos Cadastrados"/>
            {confirmation.isShow ? (
                <Confirmation data={confirmation} fnc={setConfirmation}/>
            ):(
           <div className="list_user">
              <Loading show={isEmpty}/>           
                {mountList()}                                                        
    </div>)}
    
        </React.Fragment>
)}
export default Itemlist