import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import Alert from '../alert/'
import './login.css'
import {authentication} from '../../../services/auth'
import {saveToken} from '../../../config/auth'
const { http } = require('../../../config/config');



const Login = (props) => {
    const history = useHistory()
    const [auth, setAuth] = useState({ })
    const [error,setError] = useState("")
    const [loading, setLoading] = useState(false)

    const handleChange = (event) => {         
        setAuth({
            ...auth,
            [event.target.name]: event.target.value
        })
        return;
    }

    const submitIsValid = () => {
        return auth.email && auth.password
    }

    const pressEnter = (event) => event.key === 'Enter' ? submitLogin() : null

    const submitLogin = async () => {        
        if(submitIsValid()) { 
            setLoading(true)
           try {
            const {data: {token}} = await authentication(auth)
            http.defaults.headers['x-auth-token'] = token;
            saveToken(token)               
            history.push('/')
           }catch (erro) {
            setLoading(false)
            const erroCurrent = erro.response.data.errors
            if(erroCurrent){
                const allItens = erroCurrent.map(item => item.msg)
                const allItensToString = allItens.join("-")
                setError(allItensToString)
            }
            console.log(erro)
           }                   
        }
        return;
    }

    const newUser = () => history.push('/user/create')
    


    return(
        
<div id="login">
                
                <div className="form_login">
                    <div>
                        <label htmlFor="auth_login">Login</label>
                        <input disabled={loading} type="email" id="auth_login" name="email" onChange={handleChange} value={auth.email || ""} placeholder="Insira seu e-mail" onKeyPress={pressEnter}/>
                    </div>
                    <div>
                        <label htmlFor="auth_password">Senha</label>
                        <input disabled={loading} type="password" id="auth_password" name="password" onChange={handleChange} value={auth.password || ""} placeholder="Insira sua senha" onKeyPress={pressEnter}/>
                    </div>                    
                    <button disabled={!submitIsValid()} onClick={submitLogin} >
                        {loading ? (<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>) : "Entrar"}
                    </button>
                    <button onClick={newUser}>Cadastrar novo usuário</button>  

                    <div className="alertLogin">               
                    <Alert show={error ? true : false} type="error" message={error}/>  
                    </div>                                
                </div>
                
</div>

)}

export default Login