import React from 'react'
import {DeleteItem} from '../../../services/jogos'

const Confirmation = (props)  => {

    const deleteItem = async (value) => {
        if(value){
        await DeleteItem(props.data.params.nome)        
        }
        props.fnc({
            isShow: false,
            params: {}
        })
    }


    return (
        <section className="boxConfirmation d-none">
            <div className="confirmation">
                <div className="message">Você deseja excluir {props.data.params.nome}?</div>
                <div className="actions">
                    <button onClick={() => deleteItem(false)} className="cancel">NÃO</button>
                    <button onClick={() => deleteItem(true)} className="success">SIM</button>
                </div>
            </div>
        </section>
    )

}

export default Confirmation