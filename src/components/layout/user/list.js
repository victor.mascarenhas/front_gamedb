import React, { useEffect, useState } from 'react'
import {listUser, DeleteUser} from '../../../services/users'
import './user.css'
import Loading from '../loading/loading'
import Nav from '../nav/nav'


const Userlist = (props) => {
    const [user, setUser] = useState([])     

    const getList = async () => {
        try{            
            const user = await listUser()
            setUser(user.data)            
        }catch (error) {
            console.log('error', error)
        }
        }
   
        const editUser = (user) => props.history.push(`/admin/edit/${user._id}`)

        const deleteUser = async ({username}) => {            
            try{
                if(window.confirm(`Você deseja excluir ${username}?`)){
                await DeleteUser(username)
                getList()
                }
            }catch(error){
                console.log(error)
            }
}

    const isEmpty = user.length === 0
    const sortList = (user) => {
        return user.sort((a, b) => {
            if (a.is_active < b.is_active){
                return 1;                
            }
            if (a.is_active > b.is_active) {
                return -1;
            }
            return 0;
        })
    }

    const mountList = () => {
        const newList = sortList(user)
        const linhas = newList.map((it, index) => (
            <tr key={index} className={it.is_active ? "" : "noActive"}>
                <td>{it._id}</td>
                <td>{it.username}</td>
                <td>{it.email}</td>
                <td>{it.is_active ? "SIM" : "NÃO"}</td>
                <td>{it.is_admin ? "SIM" : "NÃO"}</td>
                <td>
                    <span onClick={() => editUser(it)}> Editar </span>
                    <span onClick={() => deleteUser(it)}>| Excluir </span>
                </td>
            </tr>
        ))
        return !isEmpty ? (
            <table border="1">
            <thead>
            <tr>
                <th>ID</th>
                <th>NOME DE USUÁRIO</th>
                <th>E-MAIL</th>
                <th>ATIVO</th>
                <th>ADMIN</th>
                <th>AÇÕES</th>
            </tr>
            </thead>
            <tbody>
                {linhas}
            </tbody>
            </table> 
        ) : ""
    }
    
        useEffect(() => {
            getList()
        }, [])

    return (
        <React.Fragment>
            <Nav name="Novo" to="/Admin/Create" lista="Jogos" active="Gerenciador de Usuários" />
           <div className="list_user">
              <Loading show={isEmpty}/>           
                {mountList()}                                                        
            </div>
        </React.Fragment>
)}
export default Userlist