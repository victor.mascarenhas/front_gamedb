import React, {useState, useEffect} from 'react'
import jwt from 'jsonwebtoken'
import {createUser, EditUser, ShowUser} from '../../../services/users'
import './user.css'
import Loading from '../loading/loading'
import Alert from '../alert'
import Nav from '../nav/nav'
import {useHistory, useParams} from 'react-router-dom'
import { getToken, isAuthenticated } from '../../../config/auth'

const UserCreate = (props) => {

const [userIsAdmin, setUserIsAdmin] = useState({})    
const [isSubmit, setIsSubmit] = useState(false)
const [isEdit, setIsEdit] = useState(false)
const [alert, setAlert] = useState({})
const history = useHistory()
const { userId } = useParams()
const [form, setForm] = useState({
    is_admin: false,
    is_active: true
})
const methodForm = isEdit ? EditUser : createUser


useEffect(() => {
    (async () => {
        if (isAuthenticated()){
        const {admin} = await jwt.decode(getToken())    
        setUserIsAdmin(admin.is_admin)}
        else{setUserIsAdmin(false)}
    })()
    return () => {}
}, [])

useEffect(() => {
    
    const getUser = async () => {
        const user = await ShowUser(userId)
        if (user.data.password) {
            delete user.data.password
        }
        setForm(user.data)
    }
    if (userId) {
        setIsEdit(true)
        getUser()
    }
},[userId])

const handleChange = (event) => { 
    const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
    const name =  event.target.name;        
    setForm({
        ...form,
        [name]: value
    })
}

const passMatch = form.password === form.conf

const formIsValid = () => {
    if(passMatch)    
    return form.username && form.email && form.password && form.conf
}

const submitForm = async (event) => {
    try{
        setIsSubmit(true)
        console.log('Enviando...', form)
        await methodForm(form)
        setForm({
            ...form,            
        })
        setAlert({
            type: "success",
            message: "Seu formulário foi enviado com sucessso!",
            show: true
        })        
        setForm({})
        setIsSubmit(false)
        setTimeout(() => {isAuthenticated() ? history.push('/admin/list') : history.push('/login') }, 1500)       
    } catch (e) {
        if(e.response.data.errors){
            var err = e.response.data.errors.map(item => item.msg)
            console.log(err)
            setIsSubmit(false)
            setAlert({
                type: "error",
                message: `${err}`,
                show: true
            })
    }}
}
    
return (
    <section>
        <Nav name={isAuthenticated() ? "Lista Usuários"  : "Voltar"} to={isAuthenticated() ? "/admin/list" : "/login"} active="Incluir/Atualizar"/>
        <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || ""} />
            <div className="create_user">
                
                <div className="form_login">
                    <div>
                        <label htmlFor="auth_username">Nome de usuário:</label>
                        <input disabled={isSubmit} type="text" id="auth_username" name="username" onChange={handleChange} value={form.username || ""} placeholder="Insira aqui seu nome de usuário."/>
                    </div>
                    <div>
                        <label htmlFor="auth_email">Endereço de e-mail:</label>
                        <input disabled={isSubmit} type="email" id="auth_email" name="email" onChange={handleChange} value={form.email || ""} placeholder="Insira aqui seu endereço de e-mail."/>
                    </div>
                    <div>
                        <label htmlFor="auth_password">Senha:</label>
                        <input disabled={isSubmit} type="password" id="auth_password" name="password" onChange={handleChange} value={form.password || ""} placeholder={isEdit ? `Atualize sua senha ` : 'Informe sua senha'}/>
                    </div>
                    <div>
                        <label htmlFor="conf">Confirme sua senha:</label>
                        <input disabled={isSubmit} type="password" id="conf" name="conf" onChange={handleChange} value={form.conf || ""} placeholder="Confirme sua senha."/>
                    </div>
                    {passMatch ? "" : <div className="confirmation">"Senha" e "Confirme sua senha" precisam coincidir</div>}
                    {userIsAdmin ? (
                    <div>
                        <label htmlFor="is_active">Usuário ativo:</label>
                        <input disabled={isSubmit} type="checkbox" id="is_active" name="is_active" onChange={handleChange} checked={form.is_active || false}/>
                    </div> 
                    ) : "" }
                    {userIsAdmin ? (
                    <div>
                        <label htmlFor="is_admin">Tornar Administrador:</label>
                        <input disabled={isSubmit} type="checkbox" id="is_admin" name="is_admin" onChange={handleChange} checked={form.is_admin || false}/>
                    </div>
                    ) : "" }                        
                    <button disabled={!formIsValid()} onClick={submitForm}>{ isEdit ? 'Atualizar' : 'Cadastrar' } </button>
                    <Loading show={isSubmit}/>                               
                </div>
                
            </div>   
            </section>     
)

}

export default UserCreate