import React from 'react'
import imgLogo from '../../../assets/img/game.png';
import './header.css'
import {removeToken, isAuthenticated} from '../../../config/auth'
import { useHistory } from 'react-router-dom';

const Header = (props) => {
    const history = useHistory()

    const logout = () => {
    removeToken()
    history.push('/login')
    }

    const hasUser =  () => {
        if (props.info && props.info.username){
            return(
                <>
                <i className="fa fa-user" /> {props.info.username} |
                </>
            )
        } 
    }

    return ( 
    <header>
        <div className="copyRight">
        <img src={imgLogo} alt="logo" />
        </div>
        <div className="title">GameDB</div>        
        <div className="profile"> 
        {hasUser()}
        {isAuthenticated() ? (
        <button className="logout" onClick={logout}> <i className="fa fa-sign-out" /> Sair</button>
        ) : ""}
        </div>        
    </header>
)}

export default Header