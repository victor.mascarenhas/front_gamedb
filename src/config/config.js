import Axios from 'axios'
import { getToken, } from '../config/auth'
import history from './history'


const http = Axios.create({
    baseURL: process.env.NODE_ENV === 'development'
    ? `http://localhost:4646`
    : process.env.REACT_APP_API
})

http.defaults.headers['Content-Type'] = 'application/json';


if(getToken()){
    http.defaults.headers['x-auth-token'] = getToken();
}


const interceptor = http.interceptors.response.use(
    response => response,
    error => {
        // Error
        const { response: { status } } = error;

        if (error.message === 'Network Error' && !error.response) {
            alert('você está sem internet')
        }

        switch (status) {
            case 401:
                console.log('401', 'sem autorização')
                // removeToken()
                history.push('/login')
                break;
            case 403:
                history.push(`/erro/403`)
                break;
            case 404:
                history.push(`/erro/404`)
                break;
            default:
                console.log("Aconteceu um erro, status code:", status)
                
                break;
        }
        Axios.interceptors.response.eject(interceptor);
        return Promise.reject(error);
    }
);

export {
    http
}