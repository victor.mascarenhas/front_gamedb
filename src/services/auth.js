const { http } = require("../config/config");

const authentication = (data) => http.post('/auth', data)

export {
    authentication
}