import {http} from '../config/config'

const createItem = (data) => http.post('/jogo/', data)
const listItem = () => http.get('/jogo/')
const DeleteItem = (nome) => http.delete(`/jogo/${nome}`)
const EditItem = (data) => http.patch(`/jogo/${data._id}`, data)
const ShowItem = (_id) => http.get(`/jogo/${_id}`)

export {
    createItem,
    DeleteItem,
    listItem,
    EditItem,
    ShowItem
}