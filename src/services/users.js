import {http} from '../config/config'

const createUser = (data) => http.post('/admin/', data)
const listUser = () => http.get('/admin/')
const DeleteUser = (username) => http.delete(`/admin/${username}`)
const EditUser = (data) => http.patch(`/admin/${data._id}`, data)
const ShowUser = (_id) => http.get(`/admin/${_id}`)

export {
    createUser,
    DeleteUser,
    listUser,
    EditUser,
    ShowUser
}