# Projeto GameDB

Cadastro de jogos eletrônicos.

## Versão

0.1.1 (beta)

## Pre requisitos

* React-App

## Scripts Disponíveis
``` yarn start ```

Inicia o aplicativo em modo de desenvolvimento na rota padrão http://localhost:3000

## Funções já implementadas

* Formulário para cadastro de jogos.
* Importação da lista de todos os jogos já cadastrados.

## Falta implementar

* Função editar/excluir jogos.
* Formulário para alterar/atualizar cadastros.
* Login de usuários.
 
*Para utilizar o aplicativo, é necessário passar pela tela de login preenchendo todos os formulários com qualquer informação, não existe validação de dados ainda.
